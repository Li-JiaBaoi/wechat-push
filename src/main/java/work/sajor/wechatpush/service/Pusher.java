package work.sajor.wechatpush.service;

import com.alibaba.fastjson.JSONObject;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import work.sajor.wechatpush.util.CaiHongPi;
import work.sajor.wechatpush.util.JiNianRi;

/**
 *@ClassName Pusher
 *@Description TODO
 *@Author ydzhao
 *@Date 2022/8/2 16:03
 */
@Service
public class Pusher {
    /**
     * 测试号的appId和secret
     */
    @Value("${wechat.appId}")
    private String appId;
    @Value("${wechat.secret}")
    private String secret;
    //模版id
    @Value("${wechat.templateId}")
    private String templateId;

    @Value("${tian.caihongpi.key}")
    private String key;

    /**
     * 恋爱
     */
    @Value("${target.lianAi}")
    private String lianAi;
    /**
     * 领证
     */
    @Value("${target.linZheng}")
    private String linZheng;
    /**
     * 结婚
     */
    @Value("${target.jieHun}")
    private String jieHun;
    /**
     * 生日
     */
    @Value("${target.shengRi}")
    private String shengRi;


    @Autowired
    Tianqi tianqiService;

    public void push(String id){
        String[] openIds = id.split("\\|");
        for (String openId : openIds) {
            //1，配置
            WxMpInMemoryConfigStorage wxStorage = new WxMpInMemoryConfigStorage();
            wxStorage.setAppId(appId);
            wxStorage.setSecret(secret);
            WxMpService wxMpService = new WxMpServiceImpl();
            wxMpService.setWxMpConfigStorage(wxStorage);
            //2,推送消息
            WxMpTemplateMessage templateMessage = WxMpTemplateMessage.builder()
                    .toUser(openId)
                    .templateId(templateId)
                    //.url("https://30paotui.com/")//点击模版消息要访问的网址
                    .build();
            //填写变量信息，比如天气之类的
            JSONObject todayWeather = tianqiService.getNanjiTianqi();
            templateMessage.addData(new WxMpTemplateData("riqi",todayWeather.getString("date") + "  "+ todayWeather.getString("week")));
            templateMessage.addData(new WxMpTemplateData("tianqi",todayWeather.getString("text_day") + "  " + todayWeather.getString("wd_day") + " " + todayWeather.getString("wc_day")));
            templateMessage.addData(new WxMpTemplateData("low",todayWeather.getString("low") + ""));
            templateMessage.addData(new WxMpTemplateData("high",todayWeather.getString("high")+ ""));
            templateMessage.addData(new WxMpTemplateData("caihongpi",CaiHongPi.getCaiHongPi(key)));
            templateMessage.addData(new WxMpTemplateData("lianai", JiNianRi.getLianAi(lianAi)+""));
            templateMessage.addData(new WxMpTemplateData("shengri",JiNianRi.getShengRi(shengRi)+""));
            String beizhu = "";
            if(JiNianRi.getLianAi(lianAi) % 365 == 0){
                beizhu = "今天是恋爱纪念日！";
            }
            templateMessage.addData(new WxMpTemplateData("beizhu",beizhu,"#FF0000"));


            try {
                wxMpService.getTemplateMsgService().sendTemplateMsg(templateMessage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
